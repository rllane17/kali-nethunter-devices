---
title: Kali NetHunter Statistics Overview
---

## Pre-created Images

- [Pre-created Images Details](images.html) <!-- Breakdown? Details? -->
- [Pre-created Images Overview](image-summary.html)

## Kernels

- [Kernel Details](kernels.html) <!-- Breakdown? Details? -->
- [Kernel Summary](kernel-summary.html)

- - -

## Links

- [Kali Linux Home](https://www.kali.org/)
- [Kali NetHunter Documentation](https://www.kali.org/docs/nethunter/)
- [Download Kali NetHunter pre-created images](https://www.kali.org/get-kali/#kali-mobile)
- [Kali NetHunter Build-Scripts](https://gitlab.com/kalilinux/nethunter/build-scripts)
- [Kali NetHunter Build-Scripts (Installer)](https://gitlab.com/kalilinux/nethunter/build-scripts/kali-nethunter-installer)
- [Kali NetHunter Build-Scripts (Kernels)](https://gitlab.com/kalilinux/nethunter/build-scripts/kali-nethunter-kernels)
- [Kali NetHunter `devices.yml`](https://gitlab.com/kalilinux/nethunter/build-scripts/kali-nethunter-kernels/-/blob/main/devices.yml)
- [Kali NetHunter Store](https://store.nethunter.com/)
